import { _decorator, Component, MeshCollider, MeshRenderer, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('PhysicsFBX')
export class PhysicsFBX extends Component {
  start() {
    const res = this.node.getComponentsInChildren(MeshRenderer);
    console.log(res);
    for (let i = 0; i < res.length; i++) {
      const collider = res[i].addComponent(MeshCollider);
      collider.mesh = res[i].mesh;
    }

    console.log(this.node);
    // this.node.getComponentsInChildren
  }

  // update(deltaTime: number) {}
}


